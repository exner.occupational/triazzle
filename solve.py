from structure import Board
from structure import Staple
from structure import PlacedTile

board = Board()
staple = Staple()


def status():
    print(staple)
    print(board)



def searchNextTiles(leftTile, upperTile = None):
    result = []
    if upperTile is not None:
        print("search tile next to " + str(leftTile) + " (left) and " + str(upperTile) + " (upper)")
    else:
        print("search tile next to " + str(leftTile) + " (left)")

    for num, t in enumerate(staple.TILES):
        if t.free:
            for s in range(3):
                if upperTile is not None:
                    pairToLeft = t.get_side(s, 2).pair
                    pairToUpper = t.get_side(s, 0).pair
                    if (pairToLeft == leftTile.get_side(1)) and (pairToUpper == upperTile.get_side(2)):
                        placedTile = PlacedTile(t, num, s)
                        result.append(placedTile)
#                    print("if ({} == {}) and (({} is None) or ({} == {}))".format(pairToLeft, leftTile.get_side(1), upperTile, pairToUpper, upperTile.get_side(2)))
                else:
                    pairToLeft = t.get_side(s, 0).pair
#                    print("if ({} == {}) and (({} is None))".format(pair, leftTile.get_side(1), upperTile))
                    if pairToLeft == leftTile.get_side(1):
                        placedTile = PlacedTile(t, num, s)
                        result.append(placedTile)
    print("len(result) = {}".format(len(result)))
    print("\n".join([str(r) for r in result]))
    return result



def processPos(row, col):
    print()
    print()
    print("processPos({}, {})".format(row, col))

    rowLen = len(board.pos)
    colLen = len(board.pos[row])
    if row == (rowLen -1) and col == (colLen -1):
        status()
        exit(0)

    if (col < (colLen - 2)) and row < (rowLen - 1):
        if col % 2 == 1 and col > 0:
            print("options = searchNextTiles(board.pos[row][col], board.pos[row - 1][col - 1])")
            if row == 2 and col == 2:
                print("leftTile: {}, upperTile: {}".replace(board.pos[row][col], board.pos[row - 1][col]))
            options = searchNextTiles(board.pos[row][col], board.pos[row - 1][col])
        else:
            print("options = searchNextTiles(board.pos[row][col])")
            options = searchNextTiles(board.pos[row][col])
        print("next col")
        newRow = row
        newCol = col + 1

        for o in options:
            staple.pull(o.num)
            board.pos[newRow][newCol] = o
            processPos(newRow, newCol)
            print("rollback1")
            board.pos[newRow][newCol] = None
            staple.push(o.num)
    else:
        if board.pos[row][col].get_side(1).pair == board.pos[row][-1].get_side(2):
            if row < (rowLen -1):
                print("next row")
                newRow = row + 1
                newCol = 0
            else:
                print("next col (last row)")
                newRow = row
                newCol = col + 1
            processPos(newRow, newCol)
            print("rollback2")



processPos(0, 0)

