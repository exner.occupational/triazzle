import pygame
pygame.init()
size = (1700, 1000)
(size_x, size_y) = size
screen = pygame.display.set_mode((size_x, size_y))
#pygame.display.set_caption('get coords')
clock = pygame.time.Clock()

#Variables
running = True
img = pygame.image.load("data/TinyTriazzle.png")
offset = (0, 0)

#Helpers
def get_offset_chg(mouse_pos):
    t = 50
    s = 10
    (ox, oy) = (0, 0)
    if mouse_pos[0] < t : ox = s
    if mouse_pos[1] < t : oy = s
    if mouse_pos[0] > (size_x - t) : ox = -s
    if mouse_pos[1] > (size_y - t) : oy = -s
    return (ox, oy)

def calc_offset(offset, chg):
    return (offset[0] + chg[0], offset[1] + chg[1]) 

#Program
while running:
    #Events
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONUP:
            # add position to list
            print((event.pos[0] - offset[0], event.pos[1] - offset[1]))
        if event.type == pygame.QUIT:
            running = False
    screen.blit(img, offset)
    pygame.display.update()
    chg = get_offset_chg(pygame.mouse.get_pos())
    offset = calc_offset(offset, chg)
    
    clock.tick(30)
