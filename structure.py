from enum import Enum
import random
from collections import deque

class SideType(Enum):
    # <frog type><body part>
    # frog type
    #   GRU ... green limbs
    #   GEH ... looks like a brain
    #   ROT ... all over red
    #   ORA ... orange black striped
    #   HBL ... light blue with black spots
    #   BGS ... blue yellow and black
    # body part
    #   H ..... rear part
    #   V ..... front part
    # Special type "NIXX": None
    NIXX = 0
    GRUH = -1
    GRUV = 1
    GEHH = -2
    GEHV = 2
    ROTH = -3
    ROTV = 3
    ORAH = -4
    ORAV = 4
    HBLH = -5
    HBLV = 5
    BGSH = -6
    BGSV = 6
    @property
    def pair(self):
        return SideType(-self.value)

class Tile:
    def __init__(self, side0, side1, side2):
        self.sides = [side0, side1, side2]
        self.free = True

    def __str__(self):
        return "|".join([x.name for x in self.sides]) + "(" + str(self.free) + ")"

    def get_side(self, sidenum, ori = 0):
        return self.sides[(sidenum + ori) % 3]

class PlacedTile:
    # (Tile, number within staple, orientation)
    # number within staple is -1 for border tiles
    # orientation: 1, 2, 3 ; 0 = top, -1 = left border, -2 = right border, -3 = bottom middle, -4 = last
    def __init__(self, tile, num, ori):
        self.tile = tile
        self.num = num
        self.ori = ori

    def __str__(self):
        return "/".join([self.get_side(i).name for i in range(3)]) + "/" + str(self.num) + "/" + str(self.ori)

    def get_side(self, sidenum):
        return self.tile.sides[(sidenum + max(self.ori, 0)) % 3]

class Staple:
    # Alle Teile in den richtigen Positionen
    # Von oben nach unten, dann rechts nach links.
    # Seiten im Uhrzeigersinn.
    # Bei Spitze nach oben beginnend links oben
    # Bei Spitze nach unten beginnend oben
    TILEDATA = [
                [SideType.HBLV, SideType.ORAV, SideType.GRUH],
                [SideType.ROTH, SideType.GRUH, SideType.GEHV],
                [SideType.GRUV, SideType.GEHH, SideType.GRUV],
                [SideType.GEHV, SideType.ROTV, SideType.BGSH],
                [SideType.GEHV, SideType.ORAV, SideType.BGSH],
                [SideType.GEHH, SideType.ROTH, SideType.ORAH],
                [SideType.ROTV, SideType.GEHH, SideType.GRUV],
                [SideType.BGSV, SideType.HBLH, SideType.GEHV],
                [SideType.HBLV, SideType.ORAV, SideType.HBLH],
                [SideType.GRUV, SideType.ROTH, SideType.GEHH],
                [SideType.BGSV, SideType.ORAH, SideType.ROTV],
                [SideType.ORAV, SideType.HBLV, SideType.ORAV],
                [SideType.GRUH, SideType.BGSV, SideType.HBLH],
                [SideType.BGSH, SideType.GRUH, SideType.GEHH],
                [SideType.HBLV, SideType.ORAV, SideType.GRUV],
                [SideType.ORAH, SideType.BGSH, SideType.GEHV]
               ]
    # make tiles rotateable
    TILEDATA = [deque(a) for a in TILEDATA]
    # rotate tiles
    for a in TILEDATA:
        a.rotate(random.randint(0, 2))
    # build tiles
    TILES = [Tile(*l) for l in TILEDATA]
    # shuffle tiles
    random.shuffle(TILES)

    def __str__(self):
        return "\n".join([str(x) for x in self.TILES])

    def __len__(self):
        return len(TILES)

    def pull(self, n):
        if self.TILES[n].free == False:
            raise Exception("Tile already pulled from the Staple")
        else:
            self.TILES[n].free = False
            return self.TILES[n]

    def push(self, n):
        if self.TILES[n].free == False:
            self.TILES[n].free = True
        else:
            raise Exception("Tile already in the Staple")

    def tile(self, n):
        return self.TILES[n]



staple = Staple()



class Board:
    pos = []
    
    def __init__(self):
        col = 1
        for row in range(6):
            self.pos.append([None] * col)
            col += 2
        
        # Position (row, col)  -->  PlacedTile
        self.pos[0][0]  = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.NIXX), -1, 0)
        self.pos[1][0]  = PlacedTile(Tile(SideType.NIXX, SideType.HBLH, SideType.NIXX), -1, -1)
        self.pos[1][-1] = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.ORAH), -1, -2)
        self.pos[2][0]  = PlacedTile(Tile(SideType.NIXX, SideType.ROTV, SideType.NIXX), -1, -1)
        self.pos[2][-1] = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.ROTH), -1, -2)
        self.pos[3][0]  = PlacedTile(Tile(SideType.NIXX, SideType.GEHH, SideType.NIXX), -1, -1)
        self.pos[3][-1] = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.ORAH), -1, -2)
        self.pos[4][0]  = PlacedTile(Tile(SideType.NIXX, SideType.GRUH, SideType.NIXX), -1, -1)
        self.pos[4][-1] = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.BGSV), -1, -2)
        self.pos[5][0]  = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.NIXX), -1, -1)
        self.pos[5][1]  = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][2]  = PlacedTile(Tile(SideType.GEHV, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][3]  = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][4]  = PlacedTile(Tile(SideType.ORAH, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][5]  = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][6]  = PlacedTile(Tile(SideType.GEHV, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][7]  = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][8]  = PlacedTile(Tile(SideType.GEHH, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][9]  = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.NIXX), -1, -3)
        self.pos[5][10] = PlacedTile(Tile(SideType.NIXX, SideType.NIXX, SideType.NIXX), -1, -4)

    def __str__(self):
        result = ""
        for row in self.pos:
            for i, placedTile in enumerate(row):
                result += str(placedTile)
                if i < len(row):
                    result += " | "
            result += "\n"
        return result



#from collections import Counter
#l = []
#for i in tiles.staple:
#    for j in i.sides:
#        l.append(j.name)

