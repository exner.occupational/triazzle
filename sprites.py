#import Image
#import ImageDraw
from PIL import Image
from PIL import ImageDraw
import pygame
import time
#import numpy


img = Image.open('data/TinyTriazzle.png')

# icoords of all tile edges (top to bottom, left to right)
coords = [
(1743, 304),
(1485, 746),
(1998, 747),
(1222, 1192),
(1733, 1190),
(2252, 1188),
(962, 1635),
(1483, 1637),
(1995, 1634),
(2507, 1638),
(706, 2072),
(1224, 2077),
(1739, 2079),
(2251, 2082),
(2760, 2082)
]

switcher = {
    0:  [0, 2, 1],
    1:  [1, 4, 3],
    2:  [1, 2, 4],
    3:  [2, 5, 4],
    4:  [3, 7, 6],
    5:  [3, 4, 7],
    6:  [4, 8, 7],
    7:  [4, 5, 8],
    8:  [5, 9, 8],
    9:  [6, 11, 10],
    10: [6, 7, 11],
    11: [7, 12, 11],
    12: [7, 8, 12],
    13: [8, 13, 12],
    14: [8, 9, 13],
    15: [9, 14, 13],
}

def get_triangle_size(coords):
    # returns (x, y, size_x, size_y)
    x1 = min(coords[0][0], coords[1][0], coords[2][0])
    x2 = max(coords[0][0], coords[1][0], coords[2][0])
    y1 = min(coords[0][1], coords[1][1], coords[2][1])
    y2 = max(coords[0][1], coords[1][1], coords[2][1])
    return (x1, y1, (x2 - x1), (y2 - y1))

def get_tile_coords(tile):
    # input: id of tile (top to bottom, left to right)
    # output: tuple of three coords
    return (coords[switcher[tile][0]], coords[switcher[tile][1]], coords[switcher[tile][2]])

def move_triangle(coords, x, y):
    return (coords[0][0] + x, coords[0][1] + y,
            coords[1][0] + x, coords[1][1] + y,
            coords[2][0] + x, coords[2][1] + y,
           )

def cut_img(img, tri):
    img_copy = img.copy()
    imgdraw = ImageDraw.Draw(img_copy)
    #tri = get_tile_coords(0)
    #print(tri)
    #tri = ((10, 10), (500, 30), (450, 450))
    (x, y, size_x, size_y) = get_triangle_size(tri)
    #print((x, y, size_x, size_y))
    imgdraw.polygon(tri)
    #transformed = src_img.transform(dst_img.size, Image.AFFINE, A)

    mask = Image.new('1', (size_x, size_y))
    maskdraw = ImageDraw.Draw(mask)
    #print("move")
    #print(move_triangle(tri, -x, -y))
    maskdraw.polygon(move_triangle(tri, -x, -y), fill=155)

    image = Image.new('RGB', (size_x, size_y))
    #print(image.size)
    #print((x, y, x + size_x, y + size_y))
    image.paste(img.crop((x, y, x + size_x, y + size_y)), mask=mask)
    #dstdraw = ImageDraw.Draw(img)
    #dstdraw.polygon(dst_tri, fill=(255,255,255))
    #dst_img.show()
    #dst_img.paste(transformed, mask=mask)
    #dst_img.show()
    return image

def pilImageToSurface(pilImage):
    return pygame.image.fromstring(
        pilImage.tobytes(), pilImage.size, pilImage.mode).convert()

if __name__ == "__main__":
    pygame.init()
    size = (800, 600)
    (size_x, size_y) = size
    screen = pygame.display.set_mode((size_x, size_y))
    clock = pygame.time.Clock()

    for i in range(0, 16):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit(0)

        tri = get_tile_coords(i)
        my_img = cut_img(img, tri)
        surface = pilImageToSurface(my_img)

        screen.fill((44, 44, 44))
        #screen.blit(surface, surface.get_rect(center = (150, 150)))
        screen.blit(surface, (30, 30))
        pygame.display.flip()

        clock.tick(0.3)

